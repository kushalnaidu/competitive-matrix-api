### LOGIC:
#for a given app id, installed in stripe
#for a given app id, not installed in stripe, but installed in paypal
#for a given app id, not installed in stripe, but installed in braintree
#for a given app id, not installed in stripe, but installed in elsewhere - Compute mathematically. Querying will be complex.

from flask import Flask, request,render_template
app = Flask(__name__)
import numpy as np

import sqlite3

#Visualization Function
#Normalizing the values for gradual transition of color - Very high variation between the lowest and highest values
def log_normalize(mat):
    #add 1 to the values to preven log error 
    mat_p1 = [[val+1 for val in row] for row in mat]

    log_mat = np.log(mat_p1)
    upper = max(map(max,log_mat))
    y = [[val/upper for val in row] for row in log_mat]
    return (y)

#Main function
def return_table_data(selected_sdks):

    conn = sqlite3.connect('database.db')
    c = conn.cursor()

    #Size of the database
    c.execute('''SELECT app_id  FROM app_sdk''')
    total_cols = len(c.fetchall())

    #Storing names for the chosen IDs for table headers
    table_headers = []
    for sdk_no in selected_sdks:
        c.execute('''SELECT name FROM sdk
                        WHERE id ='''+str(sdk_no))
        table_headers.append(c.fetchall()[0][0])
    table_headers.append('None')


    ## MAIN CODE ##
    #storing a square matrix with values
    #Running SQL commands for each of the conditions in a loop
    rows = []
    last_row = []
    app_urls = []
    last_row_urls = []
    for i in selected_sdks:
        row = []
        row_urls = []
        for j in selected_sdks:
            if i == j:
                c.execute('''SELECT app_id  FROM app_sdk
                                WHERE (sdk_id = '''+str(i)+''' AND installed = 1)''')
            else:
                c.execute('''SELECT app_id  FROM app_sdk
                        WHERE (sdk_id = '''+str(i)+''' AND installed = 0)
                        INTERSECT
                        SELECT app_id from app_sdk
                        WHERE (sdk_id = '''+str(j)+''' AND installed = 1)''')

            resp = c.fetchall()  
            resp_ids = [x[0] for x in resp]

            if len(resp_ids) == 1:
                resp_ids += resp_ids
            c.execute('''SELECT artwork_large_url  FROM app
                WHERE ID in '''+str(tuple(resp_ids))+'''
                ORDER BY five_star_ratings DESC''')

            row_urls.append(c.fetchall()[:3])

            row.append(len(resp))
        other_sdks = selected_sdks.copy()
        other_sdks.remove(i)
        c.execute('''SELECT app_id FROM app_sdk
                        WHERE (sdk_id = '''+str(i)+''' AND installed = 0) 
                    EXCEPT 
                    SELECT app_id FROM app_sdk
                        WHERE (sdk_id IN'''+str(tuple(other_sdks))+''' AND installed = 1)''')
        resp = c.fetchall()   

        resp_ids = [x[0] for x in resp]
        c.execute('''SELECT artwork_large_url  FROM app
            WHERE ID in '''+str(tuple(resp_ids))+'''
            ORDER BY five_star_ratings DESC''')

        row_urls.append(c.fetchall()[:3]) 
        app_urls.append(row_urls)

        row.append(len(resp))
        rows.append(row)


        c.execute('''SELECT app_id FROM app_sdk
                WHERE (sdk_id = '''+str(i)+''' AND installed = 1) 
            EXCEPT 
            SELECT app_id FROM app_sdk
                WHERE (sdk_id IN'''+str(tuple(other_sdks))+''' AND installed = 0)''')

        resp = c.fetchall()  


        resp_ids = [x[0] for x in resp]
        c.execute('''SELECT artwork_large_url  FROM app
            WHERE ID in '''+str(tuple(resp_ids))+'''
            ORDER BY five_star_ratings DESC''')

        last_row_urls.append(c.fetchall()[:3]) 

        last_row.append(len(resp))


    c.execute('''SELECT app_id FROM app_sdk
                            WHERE (sdk_id NOT IN (2081, 33, 875))''')
    resp = c.fetchall()  


    resp_ids = [x[0] for x in resp]
    c.execute('''SELECT artwork_large_url  FROM app
        WHERE ID in '''+str(tuple(resp_ids))+'''
        ORDER BY five_star_ratings DESC''')

    #row_urls.append(c.fetchall()[:3]) 
    last_row_urls.append(c.fetchall()[:3]) 

    last_row.append(len(resp))
    rows.append(last_row)
    app_urls.append(last_row_urls)
    #Visualization code:
    #Compute transparency values for each cell
    alpha_vals = log_normalize(rows)

    return table_headers,rows,alpha_vals, app_urls

@app.route('/')
def hello():
    selected_sdks = list(map(int,request.args.get('ids').split(',')))
    table_headers,rows,alpha_vals,app_urls = return_table_data(selected_sdks)
    return render_template('content.html', size = len(selected_sdks)+1, rows = rows,headers = table_headers, alpha_vals = alpha_vals, app_urls = app_urls)

if __name__ == '__main__':
    app.run(debug=True)