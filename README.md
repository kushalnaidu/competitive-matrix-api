# Competitive Matrix API

Project built using Flask. 

Dependencies are listed in requirements.txt. To install the dependencies, run 
```sh
pip install -r requirements.txt
```

To execute the code, from the root directory of the project, run 
```sh
python flask-API.py
```

To build a matrix, visit http://127.0.0.1:5000/?ids= , and add SDK ID's to the end of the URL, seperated by commas. 

Example URL: http://127.0.0.1:5000/?ids=875,33,2081



Sample Image:

![Sample Image](sample_img.png)